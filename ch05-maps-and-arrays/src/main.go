package main

import (
	"fmt"
	"sort"
)

type NameAge struct {
	Name string
	Age  int
}

func main() {
	extractUniqElements()
	findElementFromArray()
	revertArray()
	iteratingArray()
	mapIntoArrayOfKeysAndValues()
	mergeArrays()
	mergeMaps()
	testForKeyInMap()
}

func testForKeyInMap() {
	nameAges := map[string]int{
		"Arno":      28,
		"Lauri":     24,
		"Alisa":     23,
		"Karoliina": 18,
	}

	fmt.Println(nameAges["Alisa"])

	value, exists := nameAges["Arno"]
	fmt.Println(value)
	fmt.Println(exists)

	value, exists = nameAges["Eduard"]
	fmt.Println(value)
	fmt.Println(exists)

	if _, exists = nameAges["Janna"]; exists {
		fmt.Println("Janna has found")
	} else {
		fmt.Println("Janna cannot found")
	}
}

func mergeMaps() {
	map1 := map[string]int{
		"Star": 11,
		"Wars": 22,
	}
	map2 := map[string]int{
		"Lord":  31,
		"Of":    32,
		"The":   33,
		"Rings": 34,
	}

	fmt.Println(map1)
	fmt.Println(map2)

	for key, value := range map2 {
		map1[key] = value
	}
	fmt.Println(map1)
}

func mergeArrays() {
	items1 := []int{1, 2}
	items2 := []int{3, 4}

	result := append(items1, items2...)
	fmt.Println(result)
}

func mapIntoArrayOfKeysAndValues() {
	var nameAgeSlice []NameAge
	nameAges := map[string]int{
		"Arno":      28,
		"Lauri":     24,
		"Alisa":     23,
		"Karoliina": 18,
	}

	for key, value := range nameAges {
		nameAgeSlice = append(nameAgeSlice, NameAge{key, value})
	}

	fmt.Println(nameAgeSlice)
}

func iteratingArray() {
	numbers := []int{1, 5, 3, 6, 2, 10, 8}

	for index, value := range numbers {
		fmt.Printf("Index: %v and value: %v\n", index, value)
	}

	for _, value := range numbers {
		fmt.Println(value)
	}
}

func revertArray() {
	numbers := []int{1, 5, 3, 6, 2, 10, 8}
	fmt.Println(numbers)

	toBeSorted := sort.IntSlice(numbers)
	sort.Sort(toBeSorted)
	fmt.Println(toBeSorted)

	sort.Sort(sort.Reverse(toBeSorted))
	fmt.Println(toBeSorted)
}

func findElementFromArray() {
	str := []string{"Arno", "Lauri", "Alisa", "Karoliina", "Janna", "Eduard"}
	for i, v := range str {
		if v == "Janna" {
			fmt.Println(i)
		}
	}

	sortedList := sort.StringSlice(str)
	sortedList.Sort()
	fmt.Println(sortedList)

	index := sortedList.Search("Janna")
	fmt.Println(index)
}

func extractUniqElements() {
	intSlice := []int{1, 5, 5, 7, 8, 6, 6, 6}
	fmt.Println(intSlice)

	uniqueSlice := unique(intSlice)
	fmt.Println(uniqueSlice)
}

func unique(intSlice []int) []int {
	keys := make(map[int]bool)
	var uniqueElements []int

	for _, entry := range intSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			uniqueElements = append(uniqueElements, entry)
		}
	}
	return uniqueElements
}
