package main

import (
	"fmt"
	"time"
)

func main() {
	findingToday()
	addingAndSubtractingDate()
	diffDates()
	parsingDateFromString()
}

func parsingDateFromString() {
	fmt.Println("===")
	str := "2019-01-07T08:32:26.371Z"
	layout := "2006-01-02T15:04:05.000Z"

	t, err := time.Parse(layout, str)

	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(t.String())
}

func diffDates() {
	fmt.Println("===")
	first := time.Date(2010, 1, 1, 0, 0, 0, 0, time.UTC)
	second := time.Date(2011, 1, 1, 0, 0, 0, 0, time.UTC)

	difference := second.Sub(first)
	fmt.Println(difference.String())
}

func addingAndSubtractingDate() {
	fmt.Println("===")
	current := time.Now()
	fmt.Println(current.String())

	anotherDate := current.AddDate(1, 1, 0)
	fmt.Println(anotherDate.String())

	// anotherDate.Sub(time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC))
	fmt.Println(anotherDate.AddDate(-1, 0, 0))
	fmt.Println(anotherDate.Add(10 * time.Minute))
	fmt.Println(anotherDate.Add(10 * time.Hour))
}

func findingToday() {
	current := time.Now()
	fmt.Println(current.String())

	fmt.Println("MM-DD-YYYY : ", current.Format("01-02-2006"))
	fmt.Println("DD/MM/YYYY : ", current.Format("02/01/2006"))
	fmt.Println("YYYY-MM-DD hh:mm:ss : ", current.Format("2006-01-02 15:04:05"))
}
