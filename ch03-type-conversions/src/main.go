package main

import (
	"fmt"
	"strconv"
)

func main() {
	convertingBooleanToString()
	convertIntAndFloatToString()
	parsingStringToBool()
	parsingStringToIntAndFloat()
	convertByteArray()
}

func convertByteArray() {
	helloWorldByte := []byte{72, 101, 108, 108, 111, 44, 32, 87, 111, 114, 108, 100}
	helloWorld := string(helloWorldByte)
	fmt.Println(helloWorld)
	fmt.Println([]byte(helloWorld))
}

func parsingStringToIntAndFloat() {
	number := "2"
	valueInt, err := strconv.Atoi(number)
	if err != nil {
		fmt.Println("Error happened.")
	} else {
		if valueInt == 2 {
			fmt.Println("success")
		}
	}

	numberFloat := "2.2"
	valueFloat, errFloat := strconv.ParseFloat(numberFloat, 64)
	if errFloat != nil {
		fmt.Println("Error happened.")
	} else {
		if valueFloat == 2.2 {
			fmt.Println("success")
		}
	}
}

func parsingStringToBool() {
	doParsing("true")
	doParsing("false")
	doParsing("1")
	doParsing("0")
	doParsing("f")
	doParsing("t")
}

func doParsing(isNew string) {
	isNewBool, err := strconv.ParseBool(isNew)
	if err != nil {
		fmt.Println("failed")
	} else {
		if isNewBool {
			fmt.Println("Is new")
		} else {
			fmt.Println("Not new")
		}
	}
}

func convertIntAndFloatToString() {
	number := int64(100)
	numberStr := strconv.FormatInt(number, 10)
	fmt.Println(numberStr)

	numberStr = strconv.Itoa(100)
	fmt.Println(numberStr)

	numberFloat := 23445621.543321
	numberFloatStr := strconv.FormatFloat(numberFloat, 'f', 5, 64)
	fmt.Println(numberFloatStr)
	numberFloatStr = strconv.FormatFloat(numberFloat, 'f', 3, 64)
	fmt.Println(numberFloatStr)
	numberFloatStr = strconv.FormatFloat(numberFloat, 'f', 1, 64)
	fmt.Println(numberFloatStr)
	numberFloatStr = strconv.FormatFloat(numberFloat, 'f', -1, 64)
	fmt.Println(numberFloatStr)
}

func convertingBooleanToString() {
	message := "Purchased item is " + strconv.FormatBool(true)
	fmt.Println(message)

	fmt.Printf("Second time %v\n", false)
}
