package main

import (
	"fmt"
	"strings"
)

func main() {
	trimString()
	extractSubstring()
	replaceString()
	escapingChars()
	capitalizeString()
}

func capitalizeString() {
	helloWorld := "hello world, how are you today!"
	helloWorldTitle := strings.Title(helloWorld)
	fmt.Println(helloWorldTitle)

	fmt.Println(strings.ToUpper(helloWorld))
}

func escapingChars() {
	helloWorld := "Hello, World, this is \"Edu\".\n\tHello again \\t"
	fmt.Println(helloWorld)
}

func replaceString() {
	helloWorld := "Hello, World. How are you World, I am good, thanks World."
	helloMars := strings.Replace(helloWorld, "World", "Mars", 1)
	fmt.Println(helloMars)

	helloMars = strings.Replace(helloWorld, "World", "Mars", 2)
	fmt.Println(helloMars)

	helloMars = strings.Replace(helloWorld, "World", "Mars", -1)
	fmt.Println(helloMars)
}

func extractSubstring() {
	greetings := "Hello, World and Mars"
	helloWorld := greetings[0:12]
	fmt.Println(helloWorld)

	fmt.Println(greetings[6:])
}

func trimString() {
	greetings := "\t Hello, World "
	fmt.Printf("%d %s\n", len(greetings), greetings)

	trimmed := strings.TrimSpace(greetings)
	fmt.Printf("%d %s\n", len(trimmed), trimmed)
}
