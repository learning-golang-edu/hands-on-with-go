package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"time"
)

type MyError struct {
	ShortMessage    string
	DetailedMessage string
}

func (e *MyError) Error() string {
	return e.ShortMessage + "\n" + e.DetailedMessage
}

func main() {
	customErrorType()
	tryCatchEquivalent()
	simpleLogging()
	handlingPanic()
	fmt.Println("After the panic was recovered")
}

func panicFunction() {
	panic("Panic!!!")
}

func handlingPanic() {
	// panic("Panic")
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in f", r)
		}
	}()
	panicFunction()
}

func simpleLogging() {
	logFile, err := os.Create("log_file")

	if err != nil {
		fmt.Println("An error occurred...")
	}

	defer logFile.Close()

	log.SetOutput(logFile)
	log.Println("Doing some logging here...")
	// next line log message and exits application
	// log.Fatalln("Fatal: application crashed!")
}

func doNothing() (string, error) {
	return "", errors.New("Something happened.")
}

func tryCatchEquivalent() {
	parsedDate, err := time.Parse("2006", "2019")

	if err != nil {
		fmt.Println("An error occurred", err.Error())
	} else {
		fmt.Println(parsedDate)
	}

	parsedDate, err = time.Parse("2006", "2019 abc")

	if err != nil {
		fmt.Println("An error occurred", err.Error())
	} else {
		fmt.Println(parsedDate)
	}

	_, err = doNothing()
	if err != nil {
		fmt.Println(err)
	}
}

func customErrorType() {
	err := doSomething()
	fmt.Println(err)
}

func doSomething() error {
	return &MyError{"Wohoo something happened", "Some meaningful details"}
}
